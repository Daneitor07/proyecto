# 3.4 Repositorios administrados
## Daniel Alberto Cota Ochoa 329701

Este trabajo fue hecho para crear distintos repositorios remotos enlazados a un mismo repositorio local

## Requisitos

- Contar con git instalado en tu sistema operativo Linux
- Tener cuenta en las distintas paginas de repositorio (Github, Gitlab, Bitbucket)
- Tener conocimientos basicos sobre git y llaves

### Instalacion

Se necesitara instalar Python3

```
https://www.python.org/download/releases/3.0/
```

## Authors
* **Daniel Cota** - 
## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


